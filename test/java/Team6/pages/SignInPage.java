package Team6.pages;

import Team6.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage {
    private  WebDriver driver;
    private  ValidateHelper validateHelper;
    private  By loginBtn = By.id("w3loginbtn");
    private   By AccountInput = By.id("modalusername");
    private    By PasswordInput = By.id("current-password");
    private  By LoginBtn = By.className("_1VfsI");

    public SignInPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void signIn(String account,String password)
    {
        validateHelper.waitforPageLoaded();
        validateHelper.clickElement(loginBtn);
        validateHelper.waitforPageLoaded();
        validateHelper.setText(AccountInput,account);
        validateHelper.setText(PasswordInput,password);
        validateHelper.clickElement(LoginBtn);

    }
}
