package Team6.testcases;

import Team6.base.ValidateHelper;
import Team6.pages.SignInPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class SignInTest {
    private static WebDriver driver;
    private static ValidateHelper validateHelper;
    private static By loginBtn = By.id("w3loginbtn");
    private static  By AccountInput = By.id("modalusername");
    private  static  By PasswordInput = By.id("current-password");
    private static By LoginBtn = By.className("_1VfsI");

    private static SignInPage signInPage;

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        validateHelper = new ValidateHelper(driver);

        signInPage = new SignInPage(driver);

        driver.get("https://www.w3schools.com/");


        signInPage.signIn("tranguyen55kg2@gmail.com","Tra12345@");


        Thread.sleep(5000);
        driver.quit();
    }
}
