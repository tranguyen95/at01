package Team6.base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.sql.Driver;

public class ValidateHelper {
    private WebDriver driver;
    private WebDriverWait wait;
    private final int timeoutWaitforPageLoaded = 20;

    public ValidateHelper(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 5);
    }

    public void setText(By element, String value)
    {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).clear();
        driver.findElement(element).sendKeys(value);
    }

    public void clickElement(By element)
    {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).click();
    }

    public void waitforPageLoaded() {
        ExpectedCondition<Boolean> expection = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete") ;
            }
        };
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeoutWaitforPageLoaded);
            wait.until(expection);
        }catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Loaded request. ");
        }
    }


}
